// fsf17day06

var express = require("express");
var path = require("path");

var app = express();


app.set("port",3000);

// routes to public
app.use(express.static(path.join(__dirname,"public")));

app.use("/libs",express.static(path.join(__dirname,"bower_components")));


app.listen(app.get("port"), function(){
    console.log("FSF17 day 06 exerciser Application started on %s at port %d",new Date(),app.get("port"));

});
